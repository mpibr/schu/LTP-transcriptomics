clc
clear variables;
close all;

% add helpers folder to path
addpath(genpath('helpers'));

% get records
fRead = fopen('../data/gene_counts/gene_counts_180117_only_mapped_with_header.txt','r');
% read first line (header)
header = fgetl(fRead);
header = strsplit(header);
header(1:2) = []; % remove Length from header, not necessary for output file

% read counts
txt = textscan(fRead,'%s %n %n %n %n %n %n %n %n %n %n %n %n %n %n %n %n %n','delimiter','\t');
fclose(fRead);
symbols = txt{1}; % gene names
length = txt{2}; % gene lengths
counts = [txt{3:end}] + 1; % counts for each sample

% remove entries with rpkm values lower than 2
idx_threshold = calculate_rpkm(counts, length);
counts(idx_threshold,:) = [];
symbols(idx_threshold) = [];

counts = calculate_deSeq(counts);

% create figures for each experiment time range (5min, 30min, 60min)
%stats05 = BinFoldChangeTest(counts(:, [2,5]), counts(:, [1,4]), 100, true);
%stats30 = BinFoldChangeTest(counts(:, [7,10]), counts(:, [6,9]), 100, true);
%stats60 = BinFoldChangeTest(counts(:, [12,15]), counts(:, [11,14,16]), 100, true);

stats05C = BinFoldChangeTest(counts(:,2),counts(:,5),100,true);
stats05T = BinFoldChangeTest(counts(:,5),counts(:,4),100,true);
%stats60A = BinFoldChangeTest(counts(:,12),counts(:,11), 100, true);
%stats60B = BinFoldChangeTest(counts(:,15),counts(:,14),100,true);
idx = (stats05C.idx_x | stats05C.idx_y | stats05T.idx_x | stats05T.idx_y);

stats05 = BinFoldChangeTest(counts(~idx, [2,5]), counts(~idx, [1,4]), 100, true);
stats05A = BinFoldChangeTest(counts(~idx,2),counts(~idx,1), 100, true);
stats05B = BinFoldChangeTest(counts(~idx,5),counts(~idx,4),100,true);

% remove helpers folder from path
rmpath('helpers');