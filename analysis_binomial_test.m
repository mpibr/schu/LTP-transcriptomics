clc
clear variables;
close all;

% add helpers folder to path
addpath(genpath('helpers'));

% get records
fRead = fopen('../data/gene_counts/gene_counts_180117_only_mapped_with_header.txt','r');
% read first line (header)
header = fgetl(fRead);
header = strsplit(header);
header(2) = []; % remove Length from header, not necessary for output file

% read counts
txt = textscan(fRead,'%s %n %n %n %n %n %n %n %n %n %n %n %n %n %n %n %n %n','delimiter','\t');
fclose(fRead);
symbols = txt{1}; % gene names
length = txt{2}; % gene lengths
counts = [txt{3:end}] + 1; % counts for each sample

% remove entries with rpkm values lower than 2
idx_threshold = calculate_rpkm(counts, length);
counts(idx_threshold,:) = [];
symbols(idx_threshold) = [];

% create figures for each experiment time range (5min, 30min, 60min)
stats05 = NBinFitTest(counts(:, [2,5]), counts(:, [1,4]), 'Constant', true);
stats30 = NBinFitTest(counts(:, [7,10]), counts(:, [6,9]), 'Constant', true);
stats60 = NBinFitTest(counts(:, [12,15]), counts(:, [11,14,16]), 'Constant', true);

% remove helpers folder from path
rmpath('helpers');