clc
clear variables;
close all;

% add helpers folder to path
addpath(genpath('helpers'));

% get all unique GO terms
fRead = fopen('data/gene_ontology/all_go_terms.txt','r');
ref_go_terms = textscan(fRead,'%s','delimiter','\t');
ref_go_terms = ref_go_terms{1};
fclose(fRead);

% get go annotation files
annot_files = dir('data/gene_ontology/go_*.txt');

go_matrix = ones(size(ref_go_terms, 1), size(annot_files, 1));
for i = 1:size(annot_files, 1)
    file = annot_files(i);
    fRead = fopen(sprintf('data/gene_ontology/%s',file.name),'r');
    content = textscan(fRead,'%s %s %s %n %n %n %n %n','delimiter','\t');
    qry_go_terms = content{1};
    p_values = content{end};
    fclose(fRead);
    
    [~, idx_ref, idx_qry] = intersect(ref_go_terms, qry_go_terms);
    
    go_matrix(idx_ref, i) = p_values(idx_qry);

end
log_matrix = log10(go_matrix);
log_matrix = abs(log_matrix);


go_table = array2table(log_matrix, 'RowNames', ref_go_terms, 'VariableNames', cellfun(@(s) strtok(s, '.'), {annot_files.name}, 'UniformOutput', false));

clustergram(log_matrix,'Standardize', 2,'cluster',1, 'ColumnLabels', cellfun(@(s) strtok(s, '.'), {annot_files.name}, 'UniformOutput', false));

% remove helpers folder from path
rmpath('helpers');