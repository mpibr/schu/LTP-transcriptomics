clc
clear variables;
close all;

% add helpers folder to path
addpath(genpath('helpers'));

% get records
fRead = fopen('../data/gene_counts/gene_counts_180117_only_mapped_with_header.txt','r');
% read first line (header)
header = fgetl(fRead);
header = strsplit(header);
header(1:2) = []; % remove Length from header, not necessary for output file

% read counts
txt = textscan(fRead,'%s %n %n %n %n %n %n %n %n %n %n %n %n %n %n %n %n %n','delimiter','\t');
fclose(fRead);
symbols = txt{1}; % gene names
length = txt{2}; % gene lengths
counts = [txt{3:end}] + 1; % counts for each sample

% remove entries with rpkm values lower than 2
idx_threshold = calculate_rpkm(counts, length);
counts(idx_threshold,:) = [];
symbols(idx_threshold) = [];

% create figures for each experiment time range (5min, 30min, 60min)
stats05 = BinFoldChangeTest(counts(:, [2,5]), counts(:, [1,4]), 100, false);
stats30 = BinFoldChangeTest(counts(:, [7,10]), counts(:, [6,9]), 100, false);
stats60 = BinFoldChangeTest(counts(:, [12,15]), counts(:, [11,14,16]), 100, false);

% idx_x contains genes meaninfully underexpressed
% idx_y contains genes meaninfully overexpressed
idx_common = stats05.idx_x | stats05.idx_y | stats30.idx_x | stats30.idx_y | stats60.idx_x | stats60.idx_y;
M = [stats05.M, stats30.M, stats60.M];
M = M(idx_common,:);

R = tiedrank(M)./size(M,1);

clustergram(R,'Standardize', 2,'cluster',1, 'ColumnLabels', {'05 min', '30 min', '60 min'});

% remove helpers folder from path
rmpath('helpers');