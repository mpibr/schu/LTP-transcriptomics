function [norm_counts,size_factors] = calculate_deSeq( raw_counts )
    % applying deSeq normalization 
    % assuming equal median between columns

    % calculate geometrical means
    geo_means = exp(mean(log(raw_counts),2));

    % calculate ratios
    ratios = bsxfun(@rdivide, raw_counts(geo_means > 0, :), geo_means(geo_means > 0));

    % calculate size_factors
    size_factors = median(ratios, 1);

    % calculate normalized counts
    norm_counts = bsxfun(@rdivide, raw_counts, size_factors);


end