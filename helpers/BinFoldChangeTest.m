function [stats] = BinFoldChangeTest(X,Y,nbins,draw)

    nrm = calculate_deSeq([X,Y]);
    Xnrm = nrm(:,1:size(X,2));
    Ynrm = nrm(:,(size(X,2)+1):end);
    A = log10(mean(nrm, 2));
    M = log2(mean(Ynrm,2)) - log2(mean(Xnrm,2));

    % create bins
    idx_bin = zeros(size(A,1),1);
    idx_bin(1:nbins:end) = 1;
    idx_bin = cumsum(idx_bin);

    % resort
    [~, idx_sort] = sort(A,'descend');
    idx_rev(idx_sort) = (1:size(idx_sort,1))';
    idx_res = false(size(A,1),1);
    Z = zeros(size(A,1),1);
    for k = 1 : max(idx_bin)
        idx_now = idx_bin == k;
        M_now =  M(idx_now(idx_rev));
        Z_now = zscore(M_now);
        idx_good = abs(Z_now) >= 1.65;
        idx_res(idx_now(idx_rev)) = idx_good;
        Z(idx_now(idx_rev)) = Z_now;
    end
    
    idx_x = idx_res & (M < median(M)) & (A >= 1);
    idx_y = idx_res & (M >= median(M)) & (A >= 1);
    idx_none = ~(idx_x | idx_y);

    if draw
        
        figure('Color','w');
        hold(gca,'on');
        plot(A(idx_none),M(idx_none), '.', 'color', [.65,.65,.65]);
        plot(A(idx_x), M(idx_x), '.', 'color', [30,144,255]./255);
        plot(A(idx_y), M(idx_y), '.', 'color', [148,0,211]./255);
        plot([min(A),max(A)],[median(M),median(M)],'k');
        hold(gca,'off');
        
        set(gca, 'box','off',...
                 'xlim',[min(A), max(A)],...
                 'ylim',[-max(abs(M)),max(abs(M))]);
        
    end
    
    % pack in structure
    stats.nrm = nrm;
    stats.A = A;
    stats.M = M;
    stats.Z = Z;
    stats.idx_x = idx_x;
    stats.idx_y = idx_y;
    stats.idx_none = idx_none;
    
end