function idx_threshold = calculate_rpkm( raw_counts, length )

    rpkm = bsxfun(@rdivide, 1e6.*raw_counts(:,2:end), sum(raw_counts(:,2:end),1));
    rpkm = bsxfun(@rdivide, 1e3.*rpkm, length);

    idx_threshold = all(rpkm < 2, 2);
end