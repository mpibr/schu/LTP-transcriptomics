function [] = plotCorrelationMatrix(cnts,label,fonts, file_name)
% plots correlation matrix

    % figure size
    fig_h = 800;
    fig_w = 800;

    % get dimensions
    dim_w = size(cnts,2);
    
    % get borders
    k = 1;
    left = 0.04;
    bottom = 0.04;
    margin = 0.01;
    width = (1 - left - dim_w*margin)/dim_w;
    height = width;
    
    % create figure
    hFig = figure('Color','w',...
       'Tag','hMainWindow',...
       'Position',[200,100,fig_w,fig_h],...
       'MenuBar','none',...
       'ToolBar','none',...
       'Name','SeqStatAnalysis',...
       'NumberTitle','off');
    hAx = zeros(dim_w*dim_w,1);

    % create idx matrix
    idx_all = (1:dim_w*dim_w);
    idx_mtx = reshape(idx_all, dim_w, dim_w);
    idx_plot = tril(idx_mtx,-1);
    idx_plot(idx_plot == 0) = [];
    idx_rsq = triu(idx_mtx,1);
    idx_rsq(idx_rsq == 0) = [];

    for i = 1 : dim_w
        for j = 1 : dim_w
            hAx(k) = axes('Parent',hFig,'Box','on',...
                      'XTick',[],'YTick',[],...
                      'Color','w','Units','normalized',...
                      'Position',[left,bottom,width,height]);
        
            y = log2(cnts(:,j)+1);
            x = log2(cnts(:,i)+1);
                  
            % plot data
            if any(idx_plot == k)
                hold(hAx(k),'on');
                plot(x,y,'.','Color',[.8,.8,.8]);
                plot([0,20],[0,20],'k');
                hold(hAx(k),'off');
                set(hAx(k),'Box','off','Layer','top',...
                       'XTick',[],'XLim',[-2,20],...
                       'YTick',[],'YLim',[-2,20]);
            end
        
            % plot histogram
            if (i == j)
                [f,x_bin] = hist(x,10);
                bar(hAx(k),x_bin,f,0.2,'EdgeColor','none','FaceColor',[.4,.4,.4]);
                set(hAx(k),'Box','off','Layer','top',...
                       'XTick',[],'XLim',[0,20],...
                       'YTick',[],'YLim',[0,10000]);
            end
            % plot rsq
            if any(idx_rsq == k)
                rsq = corr(x,y);
                text(0.5,0.5,sprintf('%.4f',rsq),'Parent',hAx(k),...
                'VerticalAlignment','middle','HorizontalAlignment','center',...
                'FontSize',fonts(2));
                set(hAx(k),'XTick',[],'XLim',[0,1],'YTick',[],'YLim',[0,1]);
            end
        
        
            % add X-Label
            if(i == 1)
                xlabel(hAx(k),label{j},'FontSize',fonts(1));
            end
             
            % add Y-Label
            if(j == 1)
                ylabel(hAx(k),label{i},'FontSize',fonts(1));
            end
    
            % update coordinates
            left = left + margin + width;
            k = k + 1;
        
        end

        % update coordinates
        left = 0.04;
        bottom = bottom + margin + height;
    end

    print(hFig,'-dpng','-r300',[file_name '.png']);

end