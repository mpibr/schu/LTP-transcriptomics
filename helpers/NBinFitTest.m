function [stats] = NBinFitTest(X, Y, vartest, plot_tf)

    nrm = calculate_deSeq([X,Y]);
    Xnrm = nrm(:,1:size(X,2));
    Ynrm = nrm(:,(size(X,2)+1):end);
    A = log10(mean(nrm, 2));
    M = log2(mean(Ynrm,2)) - log2(mean(Xnrm,2));
    
    t = nbintest(X, Y, 'VarianceLink', vartest);
    idx_x = (t.pValue <= 0.05) & (M < median(M));
    idx_y = (t.pValue <= 0.05) & (M >= median(M));
    idx_none = ~(idx_x | idx_y);

    if plot_tf
        
        figure('Color','w');
        hold(gca,'on');
        plot(A(idx_none),M(idx_none), '.', 'color', [.65,.65,.65]);
        plot(A(idx_x), M(idx_x), '.', 'color', [30,144,255]./255);
        plot(A(idx_y), M(idx_y), '.', 'color', [148,0,211]./255);
        plot([min(A),max(A)],[median(M),median(M)],'k');
        hold(gca,'off');
        
        set(gca, 'box','off',...
                 'xlim',[min(A), max(A)],...
                 'ylim',[-max(abs(M)),max(abs(M))]);
        
    end
    
    % pack in structure
    stats.nrm = nrm;
    stats.A = A;
    stats.M = M;
    stats.p = t.pValue;
    stats.idx_x = idx_x;
    stats.idx_y = idx_y;
    stats.idx_none = idx_none;
    
end

