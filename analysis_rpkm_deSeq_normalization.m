clc
clear variables;
close all;

% add helpers folder to path
addpath(genpath('helpers'));

% get records
fRead = fopen('../data/gene_counts/gene_counts_180117_only_mapped_with_header.txt','r');
% read first line (header)
header = fgetl(fRead);
header = strsplit(header);
header(2) = []; % remove Length from header, not necessary for output file

% read counts
txt = textscan(fRead,'%s %n %n %n %n %n %n %n %n %n %n %n %n %n %n %n %n %n','delimiter','\t');
fclose(fRead);
symbols = txt{1}; % gene names
length = txt{2}; % gene lengths
counts = [txt{3:end}]; % counts for each sample

% remove entries with rpkm values lower than 2
idx_threshold = calculate_rpkm(counts, length);
counts(idx_threshold,:) = [];
symbols(idx_threshold) = [];

% apply deSeq normalization to counts
normalized_counts = calculate_deSeq(counts);

% get normalized list of gene counts
normalized_cell = [symbols num2cell(normalized_counts)];
% print list to text file
normalized_table = cell2table(normalized_cell, 'VariableNames', header);
% create file
writetable(normalized_table, 'rpkm_and_deSeq_normalized_data.txt', 'Delimiter','\t');

% plot correlation matrix
header(1) = []; % remove gene ids column from header
plotCorrelationMatrix(normalized_counts, header, [16,16], 'correlation_matrix_rpkmBigger2_and_deSeq');

% remove helpers folder from path
rmpath('helpers');